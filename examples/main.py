import json
import socket
import time
from machine import WDT
import dht
import machine
import network
import webrepl

def do_lewei(t1,h1,err_cnt):
    host = 'tcp.lewei50.com'
    port = 9960
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        print('Failed to create socket. Error code: ' + str(msg[0]) + ' , Error message : ' + msg[1])
    ip_port = socket.getaddrinfo(host,port)[0][-1]
    try:
        s.connect(ip_port)
        send_data(s,t1,h1,err_cnt)
        s.close()
    except socket.error:
        print('Connect error!')
def do_connect():
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        # wlan.connect('Redmi_E07A', '12061206')
        wlan.connect('Shanyu_A58199', '12345678')
        while not wlan.isconnected():
            pass
    # print('network config:', wlan.ifconfig())
def send_data(s,t1,h1,err_cnt):
    Userkey = r'1cea97258f6943ea8d72e1be0be35009'
    msg = {'method':'update','gatewayNo':'02','userkey':Userkey}
    msg = json.dumps(msg) + '&^!'
    # print(msg)
    s.sendall(msg)
    msg1 = [{'Name':'T1','Value':t1},{'Name':'H1','Value':h1},{'Name':'Vol','Value':err_cnt}]
    msg = {'method':'upload','data': msg1}
    msg = json.dumps(msg) + '&^!'
    # print(msg)
    s.sendall(msg)
#程序开始位置...
#wlan = network.WLAN(network.STA_IF)
pin = machine.Pin(2,machine.Pin.OUT)
pin.value(0)
time.sleep(0.5)
pin.value(1)
err_cnt = -1 
d = dht.DHT22(machine.Pin(14))
#do_connect()
#webrepl.start()
print('Begin...')
wdt = WDT()
sec = 60
while True:
    sec += 1
    if sec<60:
        wdt.feed()
        time.sleep(1)
        continue
    else:
        sec = 0
        try:
            d.measure()
        except:
            err_cnt += 1
            pass
        else:
            err_cnt -= 1
            do_lewei(d.temperature(),d.humidity(),err_cnt)
            print(d.temperature(),d.humidity(),err_cnt)
# print('Finished!')
